# SPDX-FileCopyrightText: 2014-2022 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: BSD-3-Clause

CONFIG += c++17
CONFIG += qt

QT += widgets

HEADERS += MainWindow.h
HEADERS += ScreenShot.h

SOURCES += main.cpp
SOURCES += MainWindow.cpp
SOURCES += ScreenShot.cpp

TARGET = endomess
