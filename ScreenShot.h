// SPDX-FileCopyrightText: 2014-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCREENSHOT_H
#define SCREENSHOT_H

#include <QGraphicsView>
#include <QVector>

class ScreenShot : public QGraphicsView
{
    Q_OBJECT

public:
    enum Action {
        None,
        Calibrate,
        Measure
    };

    explicit ScreenShot();
    void setAction(Action action);
    double lineLength() const;
    Action action() const;
    void clearLine();

signals:
    void drawingFinished();

protected:
    void mousePressEvent(QMouseEvent *event) override;

private: // Variables
    Action m_action = Action::None;
    QVector<QPointF> m_line;
    QPointF m_lastPoint;
    QPen m_pen;

};

#endif // SCREENSHOT_H
