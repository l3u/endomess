# endomess

## Do rule of three measurements on screen shots

This little program lets you take a screenshot of what's behind it on the screen (pseudo-transparency) and do rule of three measurements on it.

Both the reference and the measuring line can consist of two or more points (each connected by a straight line).
