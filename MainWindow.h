// SPDX-FileCopyrightText: 2014-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLocale>

class ScreenShot;
class QPushButton;
class QDoubleSpinBox;
class QLabel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow();

private slots:
    void getScreenShot();
    void clearScreenShot();
    void calibrate();
    void drawingFinished();
    void setCalibration();
    void measure();

private: // Variables
    QLocale m_locale;
    ScreenShot *m_screenShot;
    QPushButton *m_clearButton;
    QPushButton *m_calibrateButton;
    QDoubleSpinBox *m_calibrationValue;
    QPushButton *m_measureButton;
    QLabel *m_measureValue;
    double m_pixelsPerMm = 0.0;

};

#endif // MAINWINDOW_H
