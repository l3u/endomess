// SPDX-FileCopyrightText: 2014-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "MainWindow.h"
#include "ScreenShot.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QDoubleSpinBox>
#include <QDebug>
#include <QTimer>
#include <QGuiApplication>
#include <QApplication>
#include <QScreen>
#include <QGraphicsPixmapItem>
#include <QLineEdit>

static const QString s_actionActive = QStringLiteral("font-weight: bold; color: green;");

MainWindow::MainWindow()
{
    auto *mainWidget = new QWidget;
    auto *mainLayout = new QVBoxLayout(mainWidget);
    setCentralWidget(mainWidget);
    setWindowTitle(tr("endomess"));

    m_screenShot = new ScreenShot;
    connect(m_screenShot, &ScreenShot::drawingFinished, this, &MainWindow::drawingFinished);
    mainLayout->addWidget(m_screenShot);

    auto *controlLayout = new QGridLayout;
    mainLayout->addLayout(controlLayout);

    auto *loadButton = new QPushButton(tr("Load"));
    connect(loadButton, &QPushButton::clicked, this, &MainWindow::getScreenShot);
    controlLayout->addWidget(loadButton, 0, 0);

    m_clearButton = new QPushButton(tr("Clear"));
    m_clearButton->setEnabled(false);
    connect(m_clearButton, &QPushButton::clicked, this, &MainWindow::clearScreenShot);
    controlLayout->addWidget(m_clearButton, 1, 0);

    m_calibrateButton = new QPushButton(tr("Calibrate"));
    m_calibrateButton->setEnabled(false);
    connect(m_calibrateButton, &QPushButton::clicked, this, &MainWindow::calibrate);
    controlLayout->addWidget(m_calibrateButton, 0, 1);

    m_calibrationValue = new QDoubleSpinBox;
    m_calibrationValue->setDecimals(1);
    m_calibrationValue->setMinimum(0.1);
    m_calibrationValue->setMaximum(100.0);
    m_calibrationValue->setPrefix(tr("Reference: "));
    m_calibrationValue->setSuffix(tr("\u2009mm"));
    m_calibrationValue->setKeyboardTracking(false);
    m_calibrationValue->setEnabled(false);
    connect(m_calibrationValue, &QDoubleSpinBox::editingFinished,
            this, &MainWindow::setCalibration);
    controlLayout->addWidget(m_calibrationValue, 1, 1);

    m_measureButton = new QPushButton(tr("Measure"));
    m_measureButton->setEnabled(false);
    connect(m_measureButton, &QPushButton::clicked, this, &MainWindow::measure);
    controlLayout->addWidget(m_measureButton, 0, 2);

    m_measureValue = new QLabel(tr("Length: -/-\u2009mm"));
    m_measureValue->setStyleSheet(QStringLiteral("font-weight: bold;"));
    controlLayout->addWidget(m_measureValue, 1, 2);
}

void MainWindow::getScreenShot()
{
    m_clearButton->setEnabled(true);
    clearScreenShot();

    hide();

    QTimer::singleShot(300, [this]
    {
        const auto ownGeometry = geometry();

        const auto styleFix = (m_screenShot->width() - m_screenShot->viewport()->width()) / 2;
        const auto screenShotGeometry = m_screenShot->frameGeometry();
        const auto screenShotTopLeft = mapToGlobal(screenShotGeometry.topLeft());

        auto screenShot = QGuiApplication::primaryScreen()->grabWindow(0,
                              screenShotTopLeft.x() + styleFix,
                              screenShotTopLeft.y() + styleFix,
                              screenShotGeometry.width(),
                              screenShotGeometry.height());

        auto *item = m_screenShot->scene()->addPixmap(screenShot);
        item->setPos(m_screenShot->mapToScene(QPoint(0, 0)));

        // Unpleasant little hack to ensure the window shows up at
        // the very position it had before hiding after re-showing
        show();
        QTimer::singleShot(25, [this, ownGeometry]
        {
            setGeometry(ownGeometry);
            calibrate();
        });
    });
}

void MainWindow::clearScreenShot()
{
    m_screenShot->scene()->clear();
    m_screenShot->clearLine();
    m_calibrateButton->setEnabled(false);
    m_calibrationValue->setEnabled(false);
    m_calibrationValue->setValue(0.1);
    m_measureButton->setEnabled(false);
}

void MainWindow::calibrate()
{
    m_calibrationValue->setEnabled(false);
    m_calibrationValue->setValue(0.1);
    m_measureButton->setEnabled(false);
    m_measureValue->setText(tr("Length: -/-\u2009mm"));
    m_screenShot->setAction(ScreenShot::Action::Calibrate);
    QTimer::singleShot(0, [this]
    {
        m_calibrateButton->setEnabled(false);
        m_calibrateButton->setStyleSheet(s_actionActive);
    });
}

void MainWindow::drawingFinished()
{
    switch (m_screenShot->action()) {
    default:
        break;
    case ScreenShot::Calibrate:
        m_calibrationValue->setEnabled(true);
        m_calibrationValue->setFocus();
        m_calibrationValue->selectAll();
        break;
    case ScreenShot::Measure:
        m_measureButton->setEnabled(true);
        m_measureButton->setStyleSheet(QString());
        const auto length = m_screenShot->lineLength() / m_pixelsPerMm;
        m_measureValue->setText(tr("Length: %1\u2009mm").arg(m_locale.toString(length, 'f', 1)));
        break;
    }
}

void MainWindow::setCalibration()
{
    // This prevents QDoubleSpinBox::editingFinished to be emitted again
    // while clearing the focus in the single show lambda call below
    m_calibrationValue->blockSignals(true);

    m_calibrateButton->setEnabled(true);
    m_calibrateButton->setStyleSheet(QString());
    m_calibrationValue->setEnabled(false);
    m_measureButton->setEnabled(true);
    m_pixelsPerMm = m_screenShot->lineLength() / m_calibrationValue->value();

    QTimer::singleShot(0, [this]
    {
        m_calibrationValue->findChild<QLineEdit *>()->deselect();
        m_calibrationValue->clearFocus();
        // Re-enable the QDoubleSpinBox::editingFinished signal
        m_calibrationValue->blockSignals(false);
    });

    measure();
}

void MainWindow::measure()
{
    m_screenShot->clearLine();
    m_measureButton->setEnabled(false);
    m_measureButton->setStyleSheet(s_actionActive);
    m_screenShot->setAction(ScreenShot::Action::Measure);
    m_measureValue->setText(tr("Length: -/-\u2009mm"));
}
