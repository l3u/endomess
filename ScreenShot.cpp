// SPDX-FileCopyrightText: 2014-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "ScreenShot.h"

#include <QDebug>
#include <QMouseEvent>
#include <QGraphicsLineItem>

#include <math.h>

static const QPointF s_noPoint = QPointF(-1, -1);

ScreenShot::ScreenShot()
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    setScene(new QGraphicsScene);
    setSceneRect(QRectF(viewport()->rect()));
}

void ScreenShot::setAction(Action action)
{
    clearLine();

    m_action = action;
    switch (action) {
    default:
        break;
    case Action::Calibrate:
        m_pen.setColor(Qt::magenta);
        break;
    case Action::Measure:
        m_pen.setColor(Qt::red);
        break;
    }
}

void ScreenShot::mousePressEvent(QMouseEvent *event)
{
    if (m_action == Action::None) {
        return;
    }

    if (event->button() == Qt::RightButton) {
        emit drawingFinished();
        return;
    }

    const auto currentPoint = mapToScene(event->pos());
    if (m_lastPoint != s_noPoint) {
        auto *line = new QGraphicsLineItem(QLineF(m_lastPoint, currentPoint));
        line->setPen(m_pen);
        scene()->addItem(line);
    }

    m_line.append(currentPoint);
    m_lastPoint = currentPoint;
}

double ScreenShot::lineLength() const
{
    if (m_line.count() < 2) {
        return 0.0;
    }

    double length = 0.0;

    for (int i = 1; i < m_line.count(); i++) {
        const auto &point1 = m_line.at(i - 1);
        const auto &point2 = m_line.at(i);
        length += sqrt(pow((point1.x() - point2.x()), 2) + pow((point1.y() - point2.y()), 2));
    }

    return length;
}

void ScreenShot::clearLine()
{
    m_line.clear();
    m_lastPoint = s_noPoint;

    QGraphicsLineItem lineItem;
    const auto lineType = lineItem.type();
    for (auto &item : items()) {
        if (item->type() == lineType) {
            scene()->removeItem(item);
        }
    }
}

ScreenShot::Action ScreenShot::action() const
{
    return m_action;
}
